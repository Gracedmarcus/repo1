using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed = 5;


    private Vector3 input;
    private Vector3 motion;

    private CharacterController controller;

    private void Awake()
    {
        controller = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        motion = Vector3.zero;
        input = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Depth"), Input.GetAxis("Vertical"));
        motion += transform.forward.normalized * input.z;
        motion += transform.right.normalized * input.x;
        motion += Vector3.up * input.y;
        controller.Move(motion * speed * Time.deltaTime);
    }
}
